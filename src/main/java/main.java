import java.util.Arrays;
import java.util.Scanner;

public class main
{
    public static void main(String[] args)
    {
        Scanner kb = new Scanner(System.in);
        int select;
        int x, y, x1, y1;
        int[] minMax = {3, 6, 2, 8, 11, 4};

        int[][] matrix1 = new int[][]{
                {2,4,6,8},
                {1,3,5,7},
                {2,4,6,8}
        };
        int[][] matrix2 = new int[][]{
                {2,1,2,1},
                {3,2,5,4},
                {6,5,4,3}
        };

        int[][] addedMatrix = new int[3][4];

        System.out.println("Please enter your choice");
        System.out.println("Select 1 for ----Q2----");
        System.out.println("Select 2 for ----Q3----");
        select = kb.nextInt();

        switch(select){
            case 1:
                addMatrices(matrix1, matrix2, addedMatrix);

            case 2:
                indexOfMinAndMax(minMax);
                System.out.println(Arrays.toString(indexOfMinAndMax(minMax)));

        }
    }

//    public static void addMatrices(int[][] matrix1, int[][] matrix2, int[][] addedMatrix)
//    {
//        for(int i = 0; i < 3; ++i)
//        {
//            for(int h = 0; h < 4; ++h)
//            {
//                addedMatrix[i][h] = matrix1[i][h] + matrix2[i][h];
//                System.out.print(addedMatrix[i][h] + " ");
//            }
//            System.out.println();
//        }
//
//    }

    //The other method was for part(i), this one will take in more general arrays


    public static void addMatrices(int[][] matrix1, int[][] matrix2, int[][] addedMatrix)
    {
        for(int i = 0; i < matrix1.length; ++i)
        {
            for(int h = 0; h < matrix1[i].length; ++h)
            {
                if(matrix1.length != matrix2.length || matrix2[i].length  != matrix1[i].length){
                    System.out.println("Please enter two matrices with the same amount of rows and columns as each other");
                    break;
                }
                addedMatrix[i][h] = matrix1[i][h] + matrix2[i][h];
                System.out.print(addedMatrix[i][h] + " ");
            }
            System.out.println();
        }

    }

    public static int[] indexOfMinAndMax(int[] minMax){
        Arrays.sort(minMax);
        int max = 0;
        for(int j = 0; j < minMax.length; ++j){
            max = minMax[j];
        }
        int min = minMax[0];
        int ans[] = {min, max};
        return ans;

    }



}
